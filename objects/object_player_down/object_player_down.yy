{
    "id": "d940f23b-dcab-4859-aa04-8352fd0a5602",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player_down",
    "eventList": [
        {
            "id": "348c342f-15af-4d66-a224-61cfddcffc97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d940f23b-dcab-4859-aa04-8352fd0a5602"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
    "visible": true
}