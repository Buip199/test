{
    "id": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68e1b655-6d43-41cf-b1f0-3dc2f76f9141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "baba52f1-c30f-4aac-8bd4-11f55eddd740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e1b655-6d43-41cf-b1f0-3dc2f76f9141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1778e1a5-1560-4d76-a571-8ced09ee16c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e1b655-6d43-41cf-b1f0-3dc2f76f9141",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        },
        {
            "id": "36c86549-dd5d-4318-a2a0-1c23bf0678a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "908fd9f1-2a72-4c85-9069-1519629757d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c86549-dd5d-4318-a2a0-1c23bf0678a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc87774-8aac-4c40-853c-61a7242d23aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c86549-dd5d-4318-a2a0-1c23bf0678a5",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        },
        {
            "id": "3e7d5eaf-ebd8-481d-9f4d-0962ebc577e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "f759274d-7045-44d4-ae4b-821465398f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e7d5eaf-ebd8-481d-9f4d-0962ebc577e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89239e96-0753-48a9-883b-10a58d8b75e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e7d5eaf-ebd8-481d-9f4d-0962ebc577e3",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        },
        {
            "id": "8fd12d99-5ed8-408a-8b3a-4badc8162193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "9d27de66-3146-4c2f-a41b-8373393965bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd12d99-5ed8-408a-8b3a-4badc8162193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c4e876f-c418-4f11-b58c-231fc30f5fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd12d99-5ed8-408a-8b3a-4badc8162193",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        },
        {
            "id": "89258186-d377-4ff9-a5cf-685553927922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "ac162bdf-b03b-4a27-9312-f16edf63fd06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89258186-d377-4ff9-a5cf-685553927922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95eff28b-4928-4a3d-9336-24c6afa64afd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89258186-d377-4ff9-a5cf-685553927922",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        },
        {
            "id": "335b2de5-b2fa-4a4f-b219-e3847449a418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "05b23ea7-b971-44ec-acd5-62319b346cf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "335b2de5-b2fa-4a4f-b219-e3847449a418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82161994-0843-4f80-ada6-61a20d2af6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "335b2de5-b2fa-4a4f-b219-e3847449a418",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        },
        {
            "id": "93e217c3-f730-45ab-bdb8-1daf3e64866d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "9da382b9-2446-4136-b7a6-aec830170ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e217c3-f730-45ab-bdb8-1daf3e64866d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad7a4d0-8742-4d02-9deb-4cddd0c040ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e217c3-f730-45ab-bdb8-1daf3e64866d",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        },
        {
            "id": "91e03b72-7edd-46a4-b03b-b71ebb81dcb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "compositeImage": {
                "id": "e1421fdb-8725-4fbc-a928-d6bf222833fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e03b72-7edd-46a4-b03b-b71ebb81dcb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c458541c-e576-4894-88c7-bee4fcf75c43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e03b72-7edd-46a4-b03b-b71ebb81dcb7",
                    "LayerId": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc6a7164-8c03-4382-9d70-b4ccdcdea7ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa52a7be-2aff-43d7-b0a4-5750e139f764",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}